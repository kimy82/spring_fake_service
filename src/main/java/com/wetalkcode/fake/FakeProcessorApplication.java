package com.wetalkcode.fake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FakeProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(FakeProcessorApplication.class, args);
	}
}
