package com.wetalkcode.fake.repository;

import org.springframework.data.repository.CrudRepository;

import com.wetalkcode.fake.model.Processed;

public interface ProcessedRepository extends CrudRepository<Processed, Long> {
}
