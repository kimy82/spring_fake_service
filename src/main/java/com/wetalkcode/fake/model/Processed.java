package com.wetalkcode.fake.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Processed {

	@Id
	private String	id;
	
	private String rawText;
	
	private String processedText;

	public Processed(String rawText, String processedText) {
		this.rawText = rawText;
		this.processedText = processedText;
	}

	public String getRawText() {
		return rawText;
	}

	public void setRawText(String rawText) {
		this.rawText = rawText;
	}

	public String getProcessedText() {
		return processedText;
	}

	public void setProcessedText(String processedText) {
		this.processedText = processedText;
	}
}
