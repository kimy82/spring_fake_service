package com.wetalkcode.fake.service;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class Processor {
	public String porcess(String text){
			return StringUtils.hasText(text) ? text.replace("a", "_replaced_") : "";
	}
}
