package com.wetalkcode.fake.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wetalkcode.fake.model.Processed;
import com.wetalkcode.fake.repository.ProcessedRepository;
import com.wetalkcode.fake.service.Processor;

@RestController
public class FakeController {

	@Autowired
	private Processor processor;
	
	@Autowired
	private ProcessedRepository processedRepository;
	
	@RequestMapping("/process")
	public Processed processText(@RequestParam("rawtext") String rawtext){
		return this.processedRepository.save(new Processed(rawtext, this.processor.porcess(rawtext)));
	}
}
