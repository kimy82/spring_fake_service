package com.wetalkcode.fake.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

public class ProcessorTest {
	private Processor processor = new Processor();
	
	@Test
	public void replacesACharacters() throws Exception {
		assertThat(this.processor.porcess("this is a text"), is(equalTo("this is _replaced_ text")));
	}
	
	@Test
	public void returnsEmpty_WhenTxtIsemptyOrNull() throws Exception {
		assertThat(this.processor.porcess(""), is(equalTo("")));
		assertThat(this.processor.porcess(null), is(equalTo("")));
	}
	
}
