# Spring boot fake service

## Endpoint
* processes text replacing a characters for _replaced_
> /process?rawtext=....
```js

{"rawText":"this is an example","processedText":"this is _replaced_n  ex_replaced_mple"}
```

## Docker
### Docker image creation
* Using maven run: _package docker:build -Ddocker.folder=src/main/docker_
### Docker image run
* docker run -p 2020:2020 -it --add-host mongo:<mongo_ip> kimy82/fakeservice

## Kubernetes
### Mongo
> kubectl create mongo-deployment.yaml
> kubectl create mongo-service.yaml

### Backend
> kubectl create fake-service-deployment.yaml
> kubectl create fake-service-service.yaml

NOTE: When running with docker we need to pass the add-host. However when inside kubernetes cluster the mongo is the name of the service for MongoDB, so it works straight on.
